# -*- coding: utf-8 -*-
from os import getenv

__version__ = f'{getenv("VERIFIER_VERSION", "1.0.0-dev")}'
