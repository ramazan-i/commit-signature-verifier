# Commit Signature Verifier
    Commit signature validation tool based on x509 PKI infrastructure usage.

Full documentation is available at [**GitLab pages**][Pages].

### Algorithm
1. **Verifier** checks commit existence.
2. **Verifier** validates signature status.
    * If commit signature is **good**, signature check is passed.
    * If status is **not good**, raise `ValidationException`.
    * If commit is **not signed**, run more checks.

    2.1. **Verifier** checks if commit is a GitLab Merge Request commit.
    * If this is a **valid GitLab merge commit**, signature check is passed.
    * In other case, run more checks.

    2.2. **Verifier** checks if unsigned commit is a web-pushed commit.
    * If commit is pushed from web, raise `ValidationException`.
3. **Verifier** reads signature data.
4. **Verifier** loads and validates certificate chain.
5. **Verifier** validates commit author.

### Installation/Usage
1. Add root CA to `/etc/gitlab/trusted-certs/`.
2. Unpack **Verifier** to `/opt`, create the following files in the same directory:
   * `push_validation.log`, `0644`, `git:git`
   * `.gitlab.config`, `0644`, `root:root`
   
       First line is current instance address, second line is admin `read_api` token.
3. Copy `pre-receive-hook.sh` to server hook directory ([**single repo**][Repo Hooks]/[**global**][Global Hooks]).
4. Update contents of pre-receive hook: root CA path, **Verifier** path (if required).
5. _[Optional]_ create `/opt/allowlist.sh` (`0644`, `root:root`) to enable exception lists.

**Note:** docker-compose installation certificate path is `$gitlab_compose_dir/config/trusted-certs`. Global hook
directory is subject to change depending on your configuration; **Verifier** tool path is to be mounted inside another
directory.

### Development
Virtual environment usage is strongly recommended.

Set up [**x509 commit signature**][X509 Signatures] to test **Verifier** against your commits.

## Building docs
1. Install `docs/requirements.txt`.
2. Build with `sphinx-build -b html docs/source/ public`.

## Localization
1. Extract text data with `sphinx-build -aEqb gettext docs/source/ docs/.gettext`.
2. Create localization data with `sphinx-intl update -p docs/.gettext/ -d docs/source/_locales/ -l $your_languare`.
3. Translate `docs/source/_locales/$your_language/LC_MESSAGES/index.po` file (i.e. with [**SmartCAT**][SmartCAT]) or any other CAT tool.
4. Test your translation with `sphinx-build -b html docs/source/ public -Dlanguage=$your_language`.

### TODO
* Switch to a proper configuration file with Graylog address, GitLab address and token parameters.
* Remove hardcoded paths to allow in-docker/custom path usage.
* Consider tag signature checking.

[Repo Hooks]: https://docs.gitlab.com/ee/administration/server_hooks.html#create-server-hooks-for-a-repository
[Global Hooks]: https://docs.gitlab.com/ee/administration/server_hooks.html#choose-a-server-hook-directory
[Pages]: https://ramazan-i.gitlab.io/commit-signature-verifier
[X509 Signatures]: https://docs.gitlab.com/ee/user/project/repository/x509_signed_commits
[SmartCAT]: https://smartcat.com/workspace